#
# Be sure to run `pod lib lint KAPullToRefresh.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |spec|
  spec.name             = "RSBPullToRefresh"
  spec.version          = "0.1.2"
  spec.summary          = "Pull to refresh control"
  spec.description      = <<-DESC
    Easy to use pull to refresh control
    DESC

  spec.homepage         = "https://bitbucket.org/rosberryteam/rsbpulltorefresh"
  spec.license          = 'MIT'
  spec.author           = { "Anton Kormakov" => "anton.kormakov@rosberry.com" }
  spec.source           = { :git => "https://bitbucket.org/rosberryteam/rsbpulltorefresh.git", :tag => spec.version.to_s }

  spec.ios.deployment_target = '7.0'
  spec.source_files = 'RSBPullToRefresh/Classes/**/*'
  spec.default_subspec = 'Base'
  spec.frameworks = 'UIKit'

  spec.subspec 'Base' do |base|
    base.source_files = 'RSBPullToRefresh/Classes/**/*'
  end

  spec.subspec 'Reactive' do |reactive|
    reactive.xcconfig =
        { 'OTHER_CFLAGS' => '$(inherited) -DRSB_RC_WITH_REACTIVE' }
    reactive.dependency 'ReactiveCocoa'
  end

end
