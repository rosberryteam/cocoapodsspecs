Pod::Spec.new do |s|

s.name                = "RSBCollectionViewManager"
s.version             = "0.1.3"
s.summary             = "iOS library for UICollectionView managing"
s.homepage            = "https://bitbucket.org/rosberryteam/rsbcollectionviewmanager"
s.license             = 'MIT'
s.author              = { "Rosberry" => "info@rosberry.com" }
s.source              = { :git => "https://bitbucket.org/rosberryteam/rsbcollectionviewmanager.git", :tag => s.version.to_s }
s.platform            = :ios, '7.0'
s.requires_arc        = true
s.source_files        = 'RSBCollectionViewManager/*.{h,m}'

s.subspec 'Protocols' do |ss|
ss.source_files       = 'RSBCollectionViewManager/Protocols/*.{h,m}'
end

s.subspec 'Items' do |ss|
ss.dependency 'RSBCollectionViewManager/Protocols'
ss.source_files       = 'RSBCollectionViewManager/Items/*.{h,m}'
end

end
