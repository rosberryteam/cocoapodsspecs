Pod::Spec.new do |s|

s.name                = "RSBCollectionViewManager"
s.version             = "0.0.1"
s.summary             = "iOS library for UICollectionView managing"
s.homepage            = "https://evgenmikhaylov@bitbucket.org/rosberryteam/rsbcollectionviewmanager"
s.license             = 'MIT'
s.author              = { "Evgeny Mikhaylov" => "evgenmikhaylov@gmail.com" }
s.source              = { :git => "https://evgenmikhaylov@bitbucket.org/rosberryteam/rsbcollectionviewmanager.git", :tag => "0.0.1" }
s.platform            = :ios, '7.0'
s.requires_arc        = true
s.source_files        = 'RSBCollectionViewManager/*.{h,m}'

s.subspec 'Protocols' do |ss|
ss.source_files       = 'RSBCollectionViewManager/Protocols/*.{h,m}'
end

s.subspec 'Items' do |ss|
ss.dependency 'RSBCollectionViewManager/Protocols'
ss.source_files       = 'RSBCollectionViewManager/Items/*.{h,m}'
end

end
