Pod::Spec.new do |s|
  s.name         = "RSBRequest"
  s.version      = "0.0.4"
  s.summary      = "RSBRequest - nice library for easy requests"
  s.description  = <<-DESC
                    RSBRequest - nice library for easy requests.
                   DESC

  s.homepage     = "https://bitbucket.org/rosberryteam/rsbrequest"
  s.license      = "MIT"
  s.author       = { "Rosberry" => "info@rosberry.com" }
  s.source       = { :git => "https://bitbucket.org/rosberryteam/rsbrequest.git", :tag => "#{s.version}" }
  s.platform     = :ios, '8.0'

  s.source_files  = "Classes/RSBRequest/RSBRequest.{h,m}", "Classes/RSBRequest/RSBRequestProtocol.h"

  s.subspec 'ErrorsHandling' do |ss|
    ss.source_files = "Classes/RSBRequest/ErrorsHandling/RSBResponseErrorsHandler.h"
  end

  s.subspec 'Headers' do |ss|
    ss.source_files = "Classes/RSBRequest/Headers/RSBBaseRequestHeaders.{h,m}", "Classes/RSBRequest/Headers/RSBRequestHeaders.h"
  end

  s.subspec 'Parameters' do |ss|
    ss.source_files = "Classes/RSBRequest/Parameters/RSBBaseRequestParameters.{h,m}", "Classes/RSBRequest/Parameters/RSBRequestParameters.h"
  end

  s.subspec 'Reachability' do |ss|
    ss.source_files = "Classes/RSBRequest/Reachability/RSBReachability.{h,m}"
  end

  s.subspec 'ResponseBuilder' do |ss|
    ss.source_files = "Classes/RSBRequest/ResponseBuilder/RSBResponseBuilder.h"
  end

  s.subspec 'Validators' do |ss|
    ss.source_files = "Classes/RSBRequest/Validators/RSBResponseValidator.h"
  end
  
  s.dependency 'AFNetworking', '~> 3.1'
  s.dependency 'EasyMapping', '~> 0.16'

end
