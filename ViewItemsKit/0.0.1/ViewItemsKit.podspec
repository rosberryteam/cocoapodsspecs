Pod::Spec.new do |s|

s.name                = 'ViewItemsKit'
s.version             = '0.0.1'
s.summary             = 'iOS library for better view managing'
s.homepage            = 'https://github.com/rosberry/ViewItemsKit.git'
s.license             = 'MIT'
s.author              = { 'Rosberry' => 'info@rosberry.com' }
s.source              = { :git => 'https://github.com/rosberry/ViewItemsKit.git', :tag => s.version.to_s }
s.platform            = :ios, '9.0'
s.requires_arc        = true

s.dependency 'SDWebImage'
s.dependency 'Framer'

s.subspec 'Factories' do |ss|
ss.source_files       = 'Classes/ViewItemsKit/Factories/*.{h,m}'
ss.dependency 'ViewItemsKit/Styles'
ss.dependency 'ViewItemsKit/Items'
end

s.subspec 'Styles' do |ss|
ss.source_files       = 'Classes/ViewItemsKit/Styles/**/*.{h,m}'
end

s.subspec 'Items' do |ss|
ss.source_files       = 'Classes/ViewItemsKit/Views/Items/**/*.{h,m}'
ss.dependency 'ViewItemsKit/Styles'
end

end
