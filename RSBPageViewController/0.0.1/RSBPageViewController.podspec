Pod::Spec.new do |s|
  s.name         = "RSBPageViewController"
  s.version      = "0.0.1"
  s.summary      = "RSBPageViewController is a replace of UIPageViewController"
  s.description  = <<-DESC
                    RSBPageViewController is a replace of UIPageViewController.
                    It works with data source and reuse view controllers. 
                    UITableView or UICollectionView do the same with cells.
                   DESC

  s.homepage     = "https://bitbucket.org/rosberryteam/rsbpageviewcontroller"
  s.license      = "MIT"
  s.author       = { "Rosberry" => "info@rosberry.com" }
  s.platform     = :ios, "8.0"
  s.source       = { :git => "https://bitbucket.org/rosberryteam/rsbpageviewcontroller.git", :tag => "#{s.version}" }
  s.source_files = "Classes", "RSBPageViewController/RSBPageViewController/*.{h,m}"
end
