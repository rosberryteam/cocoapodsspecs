Pod::Spec.new do |s|
  s.name             = 'RSBUndoManager'
  s.version          = '0.1.1'
  s.summary          = 'Lightweight extension of NSUndoManager w/ keypath-based undo/redo registration & memory awareness.'
  s.description      = s.summary
  s.homepage         = 'https://bitbucket.org/rosberryteam/rsbundomanager'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Anton Kormakov' => 'anton.kormakov@rosberry.com' }
  s.source           = { :git => 'https://bitbucket.org/rosberryteam/rsbundomanager.git', :tag => s.version.to_s }

  s.ios.deployment_target = '6.0'

  s.source_files = 'RSBUndoManager/Classes/**/*'
end
