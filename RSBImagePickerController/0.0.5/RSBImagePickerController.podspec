#
# Be sure to run `pod lib lint RSBImagePickerController.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'RSBImagePickerController'
  s.version          = '0.0.5'
  s.summary          = 'Simple image picker controller'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
Long description of pretty simple image picker controller
                       DESC

  s.homepage         = 'https://bitbucket.org/rosberryteam/rsbimagepickercontroller'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Anton Kormakov' => 'anton.kormakov@rosberry.com' }
  s.source           = { :git => 'https://bitbucket.org/rosberryteam/rsbimagepickercontroller.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'RSBImagePickerController/Classes/**/*'
  
  s.resource_bundles = {
     'RSBImagePickerController' => ['RSBImagePickerController/Assets/*.png']
  }

  s.public_header_files = ['RSBImagePickerController/Classes/RSBImagePickerController.h', 'RSBImagePickerController/Classes/RSBImagePickerViewControllerDelegate.h']
  s.frameworks = 'UIKit'
end
