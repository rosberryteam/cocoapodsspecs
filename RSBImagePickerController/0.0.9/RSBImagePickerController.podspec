Pod::Spec.new do |s|
  s.name             = 'RSBImagePickerController'
  s.version          = '0.0.9'
  s.summary          = 'Simple image picker controller'

  s.description      = <<-DESC
Long description of pretty simple image picker controller
                       DESC

  s.homepage         = 'https://bitbucket.org/rosberryteam/rsbimagepickercontroller'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Rosberry' => 'info@rosberry.com' }
  s.source           = { :git => 'https://bitbucket.org/rosberryteam/rsbimagepickercontroller.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'RSBImagePickerController/Classes/**/*'
  
  s.resource_bundles = {
     'RSBImagePickerController' => ['RSBImagePickerController/Assets/*.png']
  }

  s.public_header_files = ['RSBImagePickerController/Classes/RSBImagePickerController.h', 'RSBImagePickerController/Classes/RSBImagePickerViewControllerDelegate.h']
  s.frameworks = 'UIKit'
end
