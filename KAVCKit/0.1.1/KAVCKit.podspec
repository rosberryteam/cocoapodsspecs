Pod::Spec.new do |s|

  s.name         = "KAVCKit"
  s.version      = "0.1.1"
  s.summary      = "KAVCKit is a set of view controllers which can replace standard UIKit navigation and tabbar controllers"

  s.description  = <<-DESC
                  Extremely customizable. You can change any part of view controllers. It works how standard view controllers
                  actually should work.
                   DESC

  s.homepage     = "http://bitbucket.org/kovalev_rosberry/kaviewcontrollerskit.git"
  
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author             = { "Anton Kovalev" => "anton.kovalev@rosberry.com" }
  
  s.platform     = :ios, "8.0"

  s.source       = { :git => "http://bitbucket.org/kovalev_rosberry/kaviewcontrollerskit.git", :tag => "0.1.1" }
  s.source_files  = "Classes", "KAVCKit/KAViewControllersKit/**/*.{h,m}"
  s.exclude_files = "Classes/Exclude"
  s.resource = 'Resources/Resources.bundle'

end
